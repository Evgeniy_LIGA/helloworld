import java.awt.*;
import java.awt.event.*;

public class Main {

    private static Thread main_loop_thread;

    public static void main(String[] args) {
        final MainLoop main_loop = new MainLoop();
        main_loop.setSize(854,680);
        Frame frame = new Frame("Пример использования import java.awt.*;");
        frame.setMinimumSize(new Dimension(320,240));
        frame.add(main_loop);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setResizable(true);
        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent we){
                main_loop.stop();
                System.exit(0);
            }
        });
        frame.setVisible(true);
        main_loop_thread = new Thread(main_loop);
        main_loop_thread.start();
    }
}
