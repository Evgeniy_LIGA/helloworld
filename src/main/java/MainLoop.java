import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.*;
import java.util.*;
import java.util.Map;
import java.awt.RenderingHints;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MainLoop extends Canvas implements Runnable, KeyListener, MouseListener, MouseMotionListener {
    private static final long serialVersionUID = 1L;	// Некий ID
    private boolean runnable;
    public int width,height;

    private VolatileImage image;
    public static boolean pause = false;

    //public static List<Entity> list = new ArrayList<Entity>();

    // Конструктор класса
    public MainLoop() {
        addKeyListener(this);
        addMouseMotionListener(this);
        addMouseListener(this);

        System.out.println("Constructor complited");
    }

    // Keyboard events

    public void keyPressed(KeyEvent e){
        System.out.println("key pressed code="+e.getKeyCode());
        if(e.getKeyCode()==80)pause=!pause;				// "p" pause
    }

    public void keyReleased(KeyEvent e){
        System.out.println("key released code="+e.getKeyCode());
    }

    public void keyTyped(KeyEvent e){System.out.println("key typed code="+e.getKeyCode());}

    // Mouse events

    public void mouseClicked(MouseEvent me){}

    public void mouseEntered(MouseEvent me){}

    public void mouseExited(MouseEvent me){}

    public void mousePressed(MouseEvent me){
        int mx = me.getX();
        int my = me.getY();
    }

    public void mouseReleased(MouseEvent me){}

    public void mouseDragged(MouseEvent me){}

    public void mouseMoved(MouseEvent me){
        double x = me.getX();
        double y = me.getY();
    }

    // Canvas rendering

    public void update(Graphics g) {}

    public void paint(Graphics g) {}

    double animation = 0.0;

    public int getPlazm(int x, int y, double offset ) {
        int diag = (int)(Math.sin(offset+(double)(y+x) / 70) * 256.0/4.0);
        int vert = (int)(Math.sin(offset+(double)(x) / 50) * 256.0/4.0);
        int horiz = (int)(Math.cos(offset+(double)(-y) / 60) * 256.0/4.0);
        int res = Math.abs(diag+horiz+vert)%256;
        return res;
    }

    public Color getSimpleGradient(int x, int y, double offset ) {
        int diag = (int)(Math.sin(offset+(double)(y+x) / 200) * 256.0/4.0);
        int vert = (int)(Math.sin(offset+(double)(x) / 200) * 256.0/4.0);
        int horiz = (int)(Math.cos(offset+(double)(-y) / 200) * 256.0/4.0);
        return new Color(Math.abs(diag*3)%256, Math.abs(vert*3)%256, Math.abs(horiz*3)%256);
    }

    public void drawBuffer() {
        if (image == null || width!=this.getWidth() || height!=this.getHeight()){
            width=this.getWidth();
            height=this.getHeight();
            image=createVolatileImage(width, height);
        }

        Graphics2D g = image.createGraphics();
        g.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        Dimension d = new Dimension(width,height);

        double offset = animation;
        int pixelization = (height+width)/100;
        if(pixelization<10)pixelization=10;
        //pixelization=5;
        for(int x=0;x<d.width;x+=pixelization){
            for(int y=0;y<d.width;y+=pixelization){
                int red = getPlazm(x,y,offset);
                int green = getPlazm(-x,y,offset);
                int blue = getPlazm(x,-y,offset);

                //int mode=(int)(animation/10)%3;
                //if(mode==0)
                    g.setColor(getSimpleGradient(x,y,offset));
                //if(mode==1)
                //    g.setColor(new Color(red,green,blue));
                //if(mode==2)
                //    g.setColor(new Color(255-red,255-green, 255-blue));

                /*int gray = (255-Math.max(red,noi2))+(255-Math.max(green,noi))+(255-Math.max(blue,noi3));
                gray/=10;
                g.setColor(new Color(gray, gray, gray));*/

                g.fillRect(x,y,pixelization,pixelization);
            }
        }

        int dx = d.width / 20;
        int dy = d.height / 20;
        //g.setColor(new Color(255,255,255,200));
        g.setColor(Color.WHITE);
        g.drawRect(dx, dy + 20, d.width -2 * dx, d.height -2 * dy -20);
        Font fl = new Font("Serif", Font.ITALIC, 2 * dy / 2);
        Font f2 = new Font ("Serif", Font.BOLD, 5 * dy / 3);
        FontMetrics fml = getFontMetrics(fl);
        FontMetrics fm2 = getFontMetrics(f2);
        String s3 = "Hello World!";
        String s1 = "Enterprise applications";
        String s2 = "on Java";
        int firstLine = (int)(d.height / 2.2);
        int nextLine = fml.getHeight();
        int secondLine = firstLine + nextLine / 2;
        g.setFont(f2);
        g.drawString(s3, (d.width-fm2.stringWidth(s3)) / 2, firstLine);
        g.drawLine(d.width / 4, secondLine -2, 3 * d.width / 4, secondLine -2);
        g.drawLine(d.width / 4, secondLine -1, 3 * d.width / 4, secondLine -1);
        g.drawLine(d.width / 4, secondLine, 3 * d.width / 4, secondLine);
        g.setFont(fl);
        g.drawString(s1, (d.width-fml.stringWidth(s1)) / 2, firstLine + 2 * nextLine);
        g.drawString(s2, (d.width-fml.stringWidth(s2)) / 2, firstLine + 3 * nextLine);
        g.dispose();


        Graphics sg = super.getGraphics();
        sg.drawImage(image, 0, 0, width, height, 0, 0, width, height, null);
        sg.dispose();
    }

    public static Random rnd = new Random(777);
    public void run() {
        System.out.println("thread runned");
        runnable=true;
        while(runnable) {
            synchronized (this){
                try {
                    drawBuffer();
                    Thread.sleep(15);
                    animation+=0.025;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        System.out.println("Thread stopped");
        runnable = false;
    }

}
